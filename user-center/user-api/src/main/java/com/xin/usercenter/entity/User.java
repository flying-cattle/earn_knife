package com.xin.usercenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author BianPeng
 * @since 2019-04-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(name = "id" , value = "用户ID")
    private Long id;

    @ApiModelProperty(name = "loginName" , value = "登录账户")
    private String loginName;

    @ApiModelProperty(name = "password" , value = "登录密码")
    private String password;

	@ApiModelProperty(name = "nickname" , value = "用户昵称")
    private String nickname;

	@ApiModelProperty(name = "type" , value = "用户类型")
    private Integer type;

	@ApiModelProperty(name = "state" , value = "用户状态")
    private Integer state;

	@ApiModelProperty(name = "note" , value = "备注")
    private String note;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "createTime" , value = "用户创建时间")
    private Date createTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "updateTime" , value = "修改时间")
    private Date updateTime;
	
	@ApiModelProperty(name = "updateUid" , value = "修改人用户ID")
    private Long updateUid;

	@ApiModelProperty(name = "loginIp" , value = "登录IP")
    private String loginIp;

	@ApiModelProperty(name = "loginIp" , value = "登录地址")
    private String loginAddr;
	
	@Override
    protected Serializable pkVal() {
        return this.id;
    }
}
