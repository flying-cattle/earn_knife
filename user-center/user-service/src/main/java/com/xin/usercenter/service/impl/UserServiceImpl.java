/**
 * @filename:UserServiceImpl 2019年4月9日
 * @project user_center  V1.0
 * Copyright(c) 2018 BianPeng Co. Ltd. 
 * All right reserved. 
 */
package com.xin.usercenter.service.impl;

import com.xin.usercenter.entity.User;
import com.xin.usercenter.dao.UserDao;
import com.xin.usercenter.service.UserService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**   
 * Copyright: Copyright (c) 2019 
 * 
 * <p>说明： 用户服务实现层</P>
 * @version: V1.0
 * @author: BianPeng
 * 
 * Modification History:
 * Date         	Author          Version          Description
 *---------------------------------------------------------------*
 * 2019年4月9日      BianPeng    V1.0           initialize
 */
@Service
public class UserServiceImpl  extends ServiceImpl<UserDao, User> implements UserService  {
	
}