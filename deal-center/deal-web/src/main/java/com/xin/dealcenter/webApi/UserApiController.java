/**
 * @filename:Const 2018年06月01日
 * @project OnlineGame    边鹏  V1.0
 * Copyright(c) 2018 BianP Co. Ltd. 
 * All right reserved. 
 */
package com.xin.dealcenter.webApi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.item.util.JsonResult;
import com.xin.usercenter.entity.User;
import com.xin.usercenter.feign.api.FeignUserApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**   
 *  
 * @Description:  user相关信息操作接口
 * @Author:       BianP   
 * @CreateDate:   2018-06-01
 * @Version:      v1.0
 *    
 */
@Api(description = "用户登录",value="用户登录信息" )
@RestController
@RequestMapping("/usercenter")
public class UserApiController {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	public FeignUserApi userService;
	
	
	/**
	 * @explain 获取指定用户   <swagger GET请求>
	 * @param   对象参数：id
	 * @return  User
	 * @author  BianP
	 */
	@ApiOperation(value = "获取用户登录信息", notes = "获取用户登录信息[user],作者：边鹏")
	@ApiImplicitParam(paramType="path", name = "id", value = "用户id", required = true, dataType = "Long")
	@GetMapping("/selectById/{id}")
	public JsonResult<User> getUserById(@PathVariable("id")Long id){
		System.out.println(id+"===================================="+id.getClass());
		return userService.selectById(id);
	}
	
	
}
