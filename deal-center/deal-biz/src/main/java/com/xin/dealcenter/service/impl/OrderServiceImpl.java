/**
 * @filename:OrderServiceImpl 2018年7月5日
 * @project deal-center  V1.0
 * Copyright(c) 2018 BianP Co. Ltd. 
 * All right reserved. 
 */
package com.xin.dealcenter.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;*/
import com.xin.dealcenter.entity.Order;
import com.xin.dealcenter.dao.OrderDao;
import com.xin.dealcenter.service.OrderService;

/**   
 *  
 * @Description:  订单——SERVICEIMPL
 * @Author:       BianP   
 * @CreateDate:   2018年7月5日
 * @Version:      V1.0
 *    
 */
@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	public OrderDao orderDao;
	
	//查询对象
	@Override
	public Order selectByPrimaryKey(Long id) {
		return orderDao.selectByPrimaryKey(id);
	}
	
	//删除对象
	@Override
	public int deleteByPrimaryKey(Long id) {
		return orderDao.deleteByPrimaryKey(id);
	}
	
	//添加对象
	@Override
	public int insertSelective(Order order) {
		return orderDao.insertSelective(order);
	}
	
	//修改对象
	@Override
	public int updateByPrimaryKeySelective(Order order) {
		return orderDao.updateByPrimaryKeySelective(order);
	}
	
	//查询集合
	@Override
	public List<Order> queryOrderList(Order order) {
		return orderDao.queryOrderList(order);
	}
	
	//分页查询
	/*	@Override
	public PageInfo<Order> getOrderBySearch(AppPage<Order> page) {
		// TODO Auto-generated method stub
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Order> list=orderDao.queryOrderList(page.getParam());
		PageInfo<Order> pageInfo = new PageInfo<Order>(list);
		return pageInfo;
	}*/
}