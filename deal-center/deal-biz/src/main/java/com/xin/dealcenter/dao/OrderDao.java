package com.xin.dealcenter.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

/*import com.github.pagehelper.PageInfo;*/
import com.xin.dealcenter.entity.Order;

@Mapper
public interface OrderDao {
	/**
	 * @explain 查询订单对象
	 * @param   对象参数：id
	 * @return  Order
	 * @author  BianP
	 */
	public Order selectByPrimaryKey(Long id);
	
	/**
	 * @explain 删除订单对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  BianP
	 */
	public int deleteByPrimaryKey(Long id);
	
	/**
	 * @explain 添加订单对象
	 * @param   对象参数：Order
	 * @return  int
	 * @author  BianP
	 */
	public int insertSelective(Order order);
	
	/**
	 * @explain 修改订单对象
	 * @param   对象参数：Order
	 * @return  int
	 * @author  BianP
	 */
	public int updateByPrimaryKeySelective(Order order);
	
	/**
	 * @explain 查询订单集合
	 * @param   对象参数：Order
	 * @return  List<Order>
	 * @author  BianP
	 */
	public List<Order> queryOrderList(Order order);
	
	/**
	 * @explain 分页查询订单
	 * @param   对象参数：Order
	 * @return  PageInfo<Order>
	 * @author  BianP
	 */
	/*public PageInfo<Order> getOrderBySearch(AppPage<Order> page);*/
}
