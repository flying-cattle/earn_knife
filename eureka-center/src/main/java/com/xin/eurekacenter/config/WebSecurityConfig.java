/**
 * @filename:Const 2018年06月01日
 * @project OnlineGame    边鹏  V1.0
 * Copyright(c) 2018 BianP Co. Ltd. 
 * All right reserved. 
 */
package com.xin.eurekacenter.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**   
 *  
 * @Description:  解决由csrf引起的Eureka服务注册失败
 * @Author:       BianP   
 * @CreateDate:   2018-06-01
 * @Version:      v1.0
 *    
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic();
    }
}
